# Dotfiles

Dotfiles are used to configure softwares

# Configure

To use dotfiles with git first initialize git on home folder

```bash
git init --bare .dotfiles
```

Then source the `init.sh` script

``` shell
source init.sh
```

Now you can run git commands with `config` alias

``` shell
config status
config add <file>
config commit -m "<message>"
config push
```
