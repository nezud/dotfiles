Invoke-Expression (&starship init powershell)

Import-Module Terminal-Icons
Import-Module PSReadLine

Set-PSReadLineKeyHandler -Key UpArrow -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Key DownArrow -Function HistorySearchForward
Set-PSReadLineKeyHandler -Key Tab -Function MenuComplete

# Aliases
Set-Alias l ls
Set-Alias grep findstr
Set-Alias vim nvim
