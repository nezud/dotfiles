#!/bin/bash
# ===========================
# Autostart script for QTile
# ===========================

picom &
setxkbmap -layout fi
unclutter --jitter 10 --ignore-scrolling --start-hidden --fork
nitrogen --restore &
